from __future__ import division
from __future__ import print_function

from typing import Dict,List,Callable,Any,Optional

import argparse
import datetime
import re
import gzip
import csv
import logging
import yaml
import os
import sys

from pathlib import Path
from xdg import XDG_CONFIG_HOME

from libpostlog.logfactory import LogFactory
from libpostlog.messagedata import MessageData
from libpostlog import logfilter
from libpostlog.logfilelocator import LogFileLocator
from libpostlog.statistics import IncomingStatistics,OutgoingStatistics, \
    TrafficStatistics
from libpostlog.configuration import AppConfiguration

def build_filter(args):
    """
    Build list of filters based on program arguments.
    Filters should be applied using logical AND.
    args {argparse.Namespace} Result of arparse.ArgumentParser.parse_args()
    return: List of LogFilter objects
    """
    result = list()
    if args.day and args.end:
        result.append(logfilter.TimeFilter(args.day, args.end))
    if args.msg_id:
        result.append(logfilter.MsgIdFilter(args.msg_id))
    if args.subject:
        result.append(logfilter.SubjectFilter(args.subject))
    if args.sender:
        result.append(logfilter.SenderFilter(args.sender))
    if args.rcpt:
        result.append(logfilter.RecipientFilter(args.rcpt))
    logging.debug("Number of filters: {}".format(len(result)))
    return result

def collect_log_data(files, filters, include_incomplete=False):

    def process_log_line(line):
        line_data = LogFactory.build_line(line)
        if line_data and len(line_data.queue_id) > 0:
            qid = line_data.queue_id
            msg = line_data.update_log_data(
                msgs[qid] if qid in msgs else MessageData()
                )
            msgs[qid] = msg
            if msg.finished:
                # Delivery completed - message removed from queue
                logging.debug("Message found: {}".format(qid))
                if filter_message(msg, filters):
                    result[qid] = msgs.pop(qid)
                else:
                    # Free item from memory to save RAM
                    msgs.pop(qid)

    result = dict()
    msgs = dict()
    for filename in files:
        logging.info("Loading {} ...".format(filename))
        if re.search(r"\.gz$", filename):
            with gzip.open(filename, "r") as f:
                for line in f:
                    lineStr = line.decode(errors="replace")
                    process_log_line(lineStr)
        else:
            with open(filename, "r", errors="replace") as f:
                for line in f:
                    process_log_line(line)

    if include_incomplete:
        # Filter messages that are still being processed in a queue
        for qid, msg in msgs.items():
            logging.debug("Message found: {}".format(qid))
            if not msg.finished and filter_message(msg, filters):
                result[qid] = msgs[qid]
    return result

def filter_message(msg, filters):
    if len(filters) == 0:
        return True
    else:
        return filters[0].match(msg) and filter_message(msg, filters[1:])

def init_config(parent_dir: str, args) -> AppConfiguration:
    # In Windows 10, the HOME variable might not be set.
    # In such cases, XDG_CONFIG_HOME contains '$HOME'.
    parent = parent_dir
    if parent[0] == '$':
        home = str(Path.home())
        parent = parent_dir.replace('$HOME', home)

    file_path = Path(parent) / 'postsendstats' / 'mailrep.conf'
    result = AppConfiguration(file_path)

    if args.log_path:
        result.log_path = args.log_path
    if args.log_mask:
        result.log_mask = args.log_mask
    if args.verbose:
        result.logging_verbosity = args.verbose
    return result

def init_logging(verbose) -> None:
    log_level = logging.ERROR
    if verbose == 1:
        log_level = logging.INFO
    elif verbose > 1:
        log_level = logging.DEBUG
    logging.basicConfig(format='%(asctime)s - %(message)s', level=log_level)

def init_stats(fn: Callable) -> Dict[str, Any]:
    result = {}
    for h in range(24):
        for m in range(60):
            key = "{0:02d}:{1:02d}".format(h, m)
            result[key] = fn()
    return result

def build_in_stats(trace) -> Dict[str, IncomingStatistics]:
    result = init_stats(IncomingStatistics)
    for item in trace.values():
        minute = item.time[:5]
        result[minute].update(item)
    return result

def build_out_stats(trace):
    result = init_stats(OutgoingStatistics)
    dlvrIter = [y for x in trace.values() for y in x.delivery]
    for dlvr in dlvrIter:
        result[dlvr.minute].update(dlvr)
    return result

def parse_cmd_args():
    ap = argparse.ArgumentParser(description="Postfix log parser")
    ap.add_argument("--log-path", help="Postfix log path")
    ap.add_argument("--log-mask", help="Log file mask")
    mode = ap.add_mutually_exclusive_group(required=True)
    mode.add_argument("-is", "--incoming-stats", action="store_true", help="mode: Incoming stats")
    mode.add_argument("-os", "--outgoing-stats", action="store_true", help="mode: Outgoing stats")
    mode.add_argument("-mt", "--msg-tracking", action="store_true", help="mode: Message tracking")
    ap.add_argument("-o", "--out", help="Output to a file")
    ap.add_argument("--append", action="store_true", help="Append instead of overwrite")
    ap.add_argument("-d", "--day",
        default=datetime.date.today().strftime("%Y-%m-%d"),
        help="Filter by date - start of date range")
    ap.add_argument("-e", "--end",
        default=(datetime.date.today() + datetime.timedelta(days=1)).strftime("%Y-%m-%d"),
        help="Filter by date - end of date range")
    ap.add_argument("-i", "--msg-id", help="Filter by message ID")
    ap.add_argument("-f", "--sender", help="Filter by sender / from")
    ap.add_argument("-t", "--rcpt", help="Filter by recipient / to")
    ap.add_argument("-s", "--subject", help="Filter by message subject")
    ap.add_argument("-v", "--verbose", action="count", default=0, help="Increase output verbosity")
    return ap.parse_args()

def write_inout_stats(stats, header: List[str], row_fn: Callable,
        file_path: Optional[str] = None,
        append=False):

    def write_stats_content(
            data: Dict[str, TrafficStatistics],
            writer,
            header: List[str],
            row_generator: Callable,
            write_header=True):
        if write_header:
            writer.writerow(header)
        for minute, item in data.items():
            row = [minute]
            row.extend(row_generator(item))
            writer.writerow(row)

    if file_path is None:
        write_stats_content(
            stats,
            csv.writer(sys.stdout, lineterminator='\n'),
            header,
            row_fn)
    else:
        file_mode = "a" if append else "w"
        write_header = not (Path(file_path).exists() and append)
        with open(file_path, file_mode, newline='') as file:
            write_stats_content(
                stats,
                csv.writer(file),
                header,
                row_fn,
                write_header)

def write_tracking(lst, file_path, append=False, csv_delim=","):
    if not file_path:
        for item in lst:
            print("Time received: {} {}".format(item.date, item.time))
            print("Subject:       {}".format(item.subject))
            print("Sender:        {}".format(item.sender))
            print("Size:          {} kB".format(item.size // 1000))
            print("Message ID:    {}".format(item.messageId))
            print("Queue ID:      {}/{}".format(item.hostname, item.queueId))
            done = "in {} s".format(item.delay) if item.finished else "not yet"
            print("Recipients: {}".format(item.rcpt_count))
            for dlvr in item.delivery:
                timeStr = "{} {}".format(dlvr.date[5:], dlvr.time[:8])
                print("    {} {} -> {} / DSN: {}".format(timeStr, dlvr.rcpt,
                    dlvr.relay, dlvr.dsn))
            print("Sent:          {}".format(done))
            print()
    else:
        # Write delivery attempts to a CSV file
        file_exists = Path(file_path).exists()
        with open(file_path, "a" if append else "w", encoding="utf-8", newline="") as file:
            # Write header line only if not append or a new file is being created
            if not (file_exists and append):
                csv_file = csv.writer(file, delimiter=csv_delim)
                csvHead = [
                    "Date",
                    "Time",
                    "Sender",
                    "Recipient",
                    "Subject",
                    "Size",
                    "NRcpt",
                    "MessageID",
                    "QueueID",
                    "DSN",
                    "Delay",
                    "ClientIP",
                    "RelayHost",
                    "Status",
                    "DeliveryFinished"
                ]
                csv_file.writerow(csvHead)
            for item in lst:
                for dlvr in item.delivery:
                    csvData = [
                        dlvr.date,
                        dlvr.time,
                        item.sender,
                        dlvr.rcpt,
                        item.subject,
                        str(item.size),
                        str(item.rcpt_count),
                        item.messageId,
                        "{}/{}".format(item.hostname, item.queueId),
                        dlvr.dsn,
                        str(dlvr.delay),
                        item.client_ip_addr,
                        dlvr.relay,
                        dlvr.status,
                        "Yes" if item.finished else "No"
                    ]
                    csv_file.writerow(csvData)
        # Done

def main():
    # TODO Read config file (from a system path, than from user path
    # overwriting previous settings)

    args = parse_cmd_args()
    cfg = init_config(str(XDG_CONFIG_HOME), args)
    init_logging(cfg.logging_verbosity)

    # FIXME Display error and exit if log path not specified

    logging.info("Range: {0} - {1}".format(args.day, args.end))

    # Build filter object based on the args
    filters = build_filter(args)

    # Parse log files
    msg_trace = collect_log_data(
        LogFileLocator.find_files(cfg.log_path, args.day, args.end,
            cfg.log_mask),
        filters,
        include_incomplete=True)

    logging.debug("Messages found: {}".format(len(msg_trace)))
    if args.incoming_stats:
        stats = build_in_stats(msg_trace)
        header = ["Minute", "Size", "Count"]
        write_inout_stats(stats, header,
            lambda x: [x.size, x.count],
            args.out, args.append)
    if args.outgoing_stats:
        stats = build_out_stats(msg_trace)
        header = ["Minute", "Size", "SentOk", "SentRetry", "SentFail", "SentTotal"]
        write_inout_stats(stats, header,
            lambda x: [x.size, x.sentOk, x.sentRetry, x.sentFail, x.sentTotal],
            args.out, args.append)
    if args.msg_tracking:
        write_tracking(msg_trace.values(), args.out, args.append)

if __name__ == "__main__":
    main()
