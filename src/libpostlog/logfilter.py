from typing import TYPE_CHECKING

import re

if TYPE_CHECKING:
    from libpostlog.messagedata import MessageData

class LogRecordFilter:
    """
    Postfix log record ('message') filter.
    Use instances of inherited classes to filter log records after
    fetched being from log completely.
    """

    def match(self, logRec: 'MessageData') -> bool:
        """
        Check if logRec matches the filter.
        Not intended to be used. Use inherited objects instead.

        Args:
            logRec (MessageData): Data about message delivery attempts 
                as collected from log file(s).
        
        Return: 
            True if match found, or False
        """
        return False

class LogStringFilter(LogRecordFilter):
    """
    General string filter.
    Used to compare string attribute of an object to specified pattern.

    Attrs:
        attribute (str): Name of attribute used in comparison
        pattern (str): Regex pattern used in in comparison (re.search())

    Return: 
            True if match found, or False
    """
    def __init__(self, attribute: str, pattern: str):
        self.attribute = attribute
        self.pattern = pattern

    def match(self, logRec: 'MessageData') -> bool:
        result = False
        if hasattr(logRec, self.attribute):
            result = re.search(self.pattern, getattr(logRec, self.attribute), 
                flags=re.IGNORECASE) != None
        return result

class MsgIdFilter(LogRecordFilter):
    """
    Message ID filter is used to filter messages with the specified
    Message-ID. Exact match is required.

    Attrs:
        id (str): Message ID used in comparison
    """

    def __init__(self, id: str):
        self.id = id

    def match(self, logRec: 'MessageData') -> bool:
        """
        Filter matches if message ID matches.
        
        Args:
            logRec (MessageData): Data about message delivery attempts 
                as collected from log file(s).

        Return: 
            True/False
        """
        result = logRec.messageId == self.id
        return result

class QueueIdFilter(LogRecordFilter):
    """
    Queue ID filter is used to filter messages with the specified
    queue ID. Exact match is required.

    Attrs:
        id (str): Message ID used in comparison
    """

    def __init__(self, id: str):
        self.id = id

    def match(self, logRec: 'MessageData') -> bool:
        """
        Filter matches if internal queue ID matches.
        
        Args:
            logRec (MessageData): Data about message delivery attempts 
                as collected from log file(s).

        Return: 
            True/False
        """
        result = logRec.queueId == self.id
        return result

class SubjectFilter(LogStringFilter):
    def __init__(self, subj: str):
        self.attribute = "subject"
        self.pattern = subj

class TimeFilter(LogRecordFilter):
    def __init__(self, timeStart="", timeEnd=""):
        self.timeStart = timeStart
        self.timeEnd = timeEnd

    def match(self, logRec: 'MessageData') -> bool:
        return self.matchTime("{} {}".format(logRec.date, logRec.time))

    def matchTime(self, timeStr: str) -> bool:
        """
        Match time if set.
        Time matches if inside the interval <timeStart; timeEnd).
        timeStr {string}: Time string (yyyy-MM-dd HH:mm:ss)
        Return True, False.
        """
        result = True
        if self.timeStart != "": 
            result = result and (timeStr >= self.timeStart)
        if self.timeEnd != "":
            result = result and (timeStr < self.timeEnd)
        return result

class SenderFilter(LogStringFilter):
    def __init__(self, sender: str):
        self.attribute = "sender"
        self.pattern = sender

class RecipientFilter(LogRecordFilter):
    def __init__(self, rcpt: str):
        self.recipient = rcpt

    def match(self, logRec: 'MessageData') -> bool:
        result = False
        for item in logRec.delivery:
            result = result or (re.search(self.recipient, item.rcpt, 
                flags=re.IGNORECASE) is not None)
        return result

if __name__ == "__main__":
    from libpostlog.messagedata import MessageData

    o = MessageData()
    o.subject("Test subject")
    f = LogStringFilter("subject", "eSt")
    print(f.match(o))
    