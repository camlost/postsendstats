import copy
from typing import Match,TYPE_CHECKING

from email.header import decode_header,make_header

from libpostlog.deliverydata import DeliveryData
from libpostlog.messagedata import MessageData

class LineData:
    """
    Common data existing in each log line.
    Individual line types should be represented by an inherited class.
    """

    PATTERN = r"^(.+)[ T](\d+:\d+:\d+[^ ]*) (\w+) ([^[]+)\[(\d+)\]: "
    
    def __init__(self, matchObj: Match = None):
        self.date = ""      # Date
        self.time = ""      # Time
        self.pid = -1       # Process ID
        self.hostname = ""  # Hostname
        self.service = ""   # Service name, ex. postfix/pickup, postfix/qmgr, ...
        if matchObj:
            self.set_values(matchObj)

    def set_values(self, matchObj: Match):
        self.date = matchObj.group(1)
        self.time = matchObj.group(2)
        self.hostname = matchObj.group(3)
        self.service = matchObj.group(4)
        self.pid = matchObj.group(5)

    def update_log_data(self, data: 'MessageData'):
        result = copy.copy(data)
        result.hostname = self.hostname
        # Update time if older than the stored one to store the earliest time
        if data.date == "?" or "{0} {1}".format(self.date, self.time) > "{0} {1}".format(data.date, data.time):
            result.date = self.date
            result.time = self.time
        return result

    @staticmethod
    def decode_mail_header(header: str):
        # If subject header has been split into several lines during transmission,
        # each line is separated be a space in the log.
        # However, the parts have to be decoded separately.
        result = header
        if header[:2] == "=?" and header[-1] == "=":
            result = ""
            for item in header.split(" "):
                parts = decode_header(item)
                # Get data (bytes) and encoding
                for data, enc in parts:
                    try:
                        if len(data) == 1 and data[0] == 63: # Skip empty strings
                            continue
                        elif enc is None:
                            result += data
                        else:
                            result += data.decode(enc)
                    except:
                        result += str(data)
        return result

class PostfixLine(LineData):
    """
    Postfix log line - a log line with a queue ID.
    """
    PATTERN = LineData.PATTERN + r"([^:]+): "
    
    def set_values(self, matchObj: Match):
        super().set_values(matchObj)
        self.queue_id = matchObj.group(6)

    def update_log_data(self, data: MessageData) -> MessageData:
        result = super().update_log_data(data)
        result.queueId = self.queue_id
        return result

class SmtpClientLine(PostfixLine):
    """
    SMTP client line contains hostname and IP address of the client host.
    """
    PATTERN = PostfixLine.PATTERN + r"client=([^\[]+)\[([^\]]+)\]$"

    def set_values(self, matchObj: Match):
        super().set_values(matchObj)
        self.hostname = matchObj.group(7)
        self.ip_address = matchObj.group(8)

    def update_log_data(self, data: MessageData) -> MessageData:
        result = super().update_log_data(data)
        result.client_ip_addr = self.ip_address
        result.client_hostname = self.hostname
        return result

class MsgIdLine(PostfixLine):
    """
    Message ID line - the one with message ID.
    Message ID is (should be) a globally unique identifier of a sent message.
    """
    PATTERN = PostfixLine.PATTERN + r"message-id=([^>]+>)$"

    def set_values(self, matchObj: Match):
        super().set_values(matchObj)
        self.message_id = matchObj.group(7)

    def update_log_data(self, data: MessageData) -> MessageData:
        result = super().update_log_data(data)
        result.messageId = self.message_id
        return result

class ConnectLine(SmtpClientLine):
    """
    Another type of SMTP client log line.
    """
    PATTERN = PostfixLine.PATTERN + r"connect from ([^[]+)\[([^\]]+)\]$"

class RemovedFromQueueLine(PostfixLine):
    """
    Log line containing message that a message was removed from queue(s).
    """
    PATTERN = PostfixLine.PATTERN + "removed$"

    def set_values(self, matchObj: Match):
        super().set_values(matchObj)
        self.removed = True

    def update_log_data(self, data: MessageData) -> MessageData:
        result = super().update_log_data(data)
        result.finished = self.removed
        return result

class InfoLine(PostfixLine):
    """
    Message details - sender, message size, number of recipients, etc.
    """
    PATTERN = PostfixLine.PATTERN + r"from=<([^>]+)>, size=([^,]+), nrcpt=([^,]+) \(queue ([^)]+)\)$"
    
    def set_values(self, matchObj: Match):
        super().set_values(matchObj)
        self.sender = matchObj.group(7)
        self.size = int(matchObj.group(8))
        self.rcpt_count = int(matchObj.group(9))
        self.queueName = matchObj.group(10)

    def update_log_data(self, data: MessageData) -> MessageData:
        result = super().update_log_data(data)
        result.sender = self.sender
        result.size = self.size
        result.rcpt_count = self.rcpt_count
        # TODO Should we store the queueName too?
        return result

class SubjectLine(PostfixLine):
    """
    Message subject logged as an INFO line.
    """
    PATTERN = PostfixLine.PATTERN + r"info: header Subject: (.+) from [^;]+; .*$"

    def set_values(self, matchObj: Match):
        super().set_values(matchObj)
        self.subject = LineData.decode_mail_header(matchObj.group(7))

    def update_log_data(self, data: MessageData) -> MessageData:
        result = super().update_log_data(data)
        result.subject = self.subject
        return result

class DeliveryLine(PostfixLine):
    """
    Delivery log line.
    It contains a lot of information - recipient's address, relay hostname an IP address, delivery status, etc.
    """
    PATTERN = PostfixLine.PATTERN + r"to=<([^>]+)>,.* relay=([^,]+), delay=([^,]+),.*, dsn=([^,]+), status=(.*)$"

    def set_values(self, matchObj: Match):
        super().set_values(matchObj)
        self.recipient = matchObj.group(7)
        self.relay = matchObj.group(8)
        self.delay = matchObj.group(9)
        self.dsn = matchObj.group(10)
        self.status = matchObj.group(11)

    def update_log_data(self, data: MessageData) -> MessageData:
        result = super().update_log_data(data)
        delivery = DeliveryData(self)
        result.add_delivery(delivery)
        return result
