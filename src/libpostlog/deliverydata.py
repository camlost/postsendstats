from typing import TYPE_CHECKING,Optional

if TYPE_CHECKING:
    from libpostlog.messagedata import MessageData
    from libpostlog.linedata import DeliveryLine
    

class DeliveryData:
    """
    Message delivery record.
    Object representing data about a delivery attempt as logged
    by the Postfix mail server. It contains rcepient's e-mail address,
    time when the attempt was made, result code (DSN), status message
    and so on.
    It is possible for a message to be delivered multiple times meaning
    there are several attempts logged.

    Attrs:
        date (str):    Date when the delivery attempt was made (yyyy-MM-dd).
        time (str):    Time of the delivery attempt (HH:mm:ss.*).
        minute (str):  Minute of the attempt (HH:mm).
        rcpt (str):    Recipient's e-mail address.
        relay (str):   Relay server (usually hostname[n.n.n.n]).
        delay (float): Time for which message spent in postfix queues so far (in seconds).
        dsn (str):     Delivery status code (n.n.n).
        status (str):  Textual information about delivery result.
    """

    def __init__(self, lineData: Optional['DeliveryLine'] = None):
        self.msgData: Optional['MessageData'] = None
        if lineData:
            self.date = lineData.date
            self.time = lineData.time
            self.minute = self.time[:5]
            self.rcpt = lineData.recipient
            self.relay = lineData.relay
            self.delay = float(lineData.delay)
            self.dsn = lineData.dsn
            self.status = lineData.status
            # TODO Set nextQueueId
        else:
            self._empty()

    def _empty(self):
        self.date = ""
        self.time = ""
        self.minute = ""    # HH:mm
        self.dsn = ""
        self.rcpt = ""
        self.relay = ""
        self.delay = 0.0
        self.status = ""
        self.nextQueueId = ""

    def __str__(self) -> str:
        """
        String representation of the data.
        Mostly for debugging purposes.
        """
        result = ""
        for attr, val in self.__dict__.items():
            result += "{} = {}\n".format(attr, val)
        return result
