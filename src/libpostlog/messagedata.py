from typing import TYPE_CHECKING, List

if TYPE_CHECKING:
    from libpostlog.deliverydata import DeliveryData

import copy

class MessageData:
    """
    Message information as collected from log files.
    Postfix log file contains information about message delivery process
    such as time, message size, number of recipients, each delivery attempt
    and so on.
    This class can be used to store such data.

    Attrs:
        date (str):            Date when was message received.
        time (str):            Time when was message received.
        hostname (str):        Hostname of computer on which was message received.
        sender (str):          Sender's e-mail address.
        size (int):            Message size in bytes.
        delay (float):         How many seconds was the message in queues.
        subject (str):         Message subject.
        queue_id (str):        Internal ID.
        messageId (str):       Unique message ID.
        client_ip_addr (str):  IP address of SMTP client which delivered message.
        client_hostname (str): Hostname of SMTP client which delivered message.
        rcpt_count (int):      Number of recipients.
        delivery (DeliveryData[]): List of delivery attempts.
        finished (bool):       True if message processing has been finished.
    """

    def __init__(self):
        self.date = "?"
        self.time = "?"
        self.hostname = ""
        self.sender = ""
        self.size = 0
        self.delay = 0
        self.subject = ""
        self.queueId = ""
        self.messageId = ""
        self.client_ip_addr = ""
        self.client_hostname = ""
        self.rcpt_count = 0
        self.delivery: List['DeliveryData'] = []
        self.finished = False

    def add_delivery(self, dlvr: 'DeliveryData'):
        """
        Add delivery data.

        Args:
            dlvr: Delivery data.
        """
        self.delivery.append(dlvr)
        if dlvr.delay > self.delay:
            self.delay = dlvr.delay
        dlvr.msgData = self
