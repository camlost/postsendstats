import glob
import os.path
import gzip
import re
from datetime import datetime,timedelta

class LogFileLocator:
    @staticmethod
    def _find_files_for_day(log_dir: str, date_str: str, file_mask: str):
        """
        Search for files in given directory containing logs from the specified date.
        Return list of file paths.
        """
        result = []
        marked_file = None
        dateLength = len(date_str)
        for item in sorted(glob.iglob(os.path.join(log_dir, file_mask))):
            fn_open = gzip.open if re.search(r"\.gz$", item) else open
            with fn_open(item, "rt", encoding="utf-8", errors="replace") as file:
                # We need to find file with date_str lines.
                # As a single file can contain both older and newer lines,
                # all but the last older file has to be skipped.
                # Newer file is skipped everytime.
                line = file.readline()
                lineDate = line[:dateLength]
                if lineDate < date_str:
                    marked_file = item
                else:
                    if marked_file is not None:
                        result.append(marked_file)
                        marked_file = None
                    if lineDate == date_str:
                        result.append(item)
        # If marked file, store marked file    
        if marked_file is not None:
            result.append(marked_file)
        return result

    @staticmethod
    def find_files(
            log_dir: str, 
            date_start: str, 
            date_end: str = (datetime.today()+timedelta(days=1)).strftime("%Y-%m-%d"), 
            file_mask: str = "*.log"):
        """
        Find files containing logs from the date interval <date_start; date_end).

        Args:
            log_dir (str)   : Log directory.
            date_start (str): Start of date range (included).
            date_end (str):   End of date range (not included).
            file_mask (str):  File mask as used by glob.

        Return:
            List of file paths sorted by time of file creation.
        """

        def daterange(start, end):
            # https://stackoverflow.com/questions/1060279/iterating-through-a-range-of-dates-in-python
            date_start = datetime.strptime(start, "%Y-%m-%d")
            date_end = datetime.strptime(end, "%Y-%m-%d")
            for day in range(int((date_end - date_start).days)):
                yield date_start + timedelta(day)
        
        file_names = set()
        for it in daterange(date_start, date_end):
            file_names.update(
                LogFileLocator._find_files_for_day(log_dir, it.strftime("%Y-%m-%d"), file_mask)
                )
        return sorted(list(file_names), key=os.path.getctime)

if __name__ == "__main__":
    lst = LogFileLocator.find_files("./log", "2019-06-14", file_mask="*mail*.*")
    print()
