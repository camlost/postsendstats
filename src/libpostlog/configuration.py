import yaml

from libpostlog.singleton import Singleton

class AppConfiguration(Singleton):
    """
    Application-wide configuration.
    """

    def init(self, file_path: str):  # type: ignore
        self.log_path = ""
        self.log_mask = ""
        self.logging_verbosity = 0

        try:
            with open(file_path) as cfg_file:
                data = yaml.load(cfg_file, Loader=yaml.FullLoader)
                self.log_mask = data['log']['mask']
                self.log_path = data['log']['path']
        except:
            # File not found
            pass

if __name__ == "__main__":
    cfg = AppConfiguration("./src/mailrep.conf")
