from libpostlog.deliverydata import DeliveryData
from libpostlog.messagedata import MessageData

class TrafficStatistics:
    """
    Common ancestor of statistical classes.
    Introduced just for type hinting purposes.
    """
    pass


class OutgoingStatistics:
    def __init__(self):
        self.sentOk = 0
        self.sentRetry = 0
        self.sentFail = 0
        self.sentTotal = 0
        self.size = 0

    def update(self, delivery: DeliveryData) -> None:
        self.sentTotal += 1
        if delivery.dsn[0] == '2':
            self.sentOk += 1
            self.size += delivery.msgData.size if delivery.msgData else 0
        elif delivery.dsn[0] == '4':
            self.sentRetry += 1
        else:
            self.sentFail += 1

class IncomingStatistics:
    """
    Simple class to store statistical data about incoming traffic.

    Attributes:
        size (int): Total size (in bytes) delivered.
        count (int): Total number of messages delivered.
    """

    def __init__(self):
        self.size = 0
        self.count = 0

    def update(self, logData: MessageData) -> None:
        """
        Update statistical data with another record as collected from logs.

        Args:
            logData (MessageData): Data collected from logs.
        """
        
        self.count += 1
        self.size += logData.size
