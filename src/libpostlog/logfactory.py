from typing import TYPE_CHECKING,Match

import re

from libpostlog.linedata import ConnectLine,SmtpClientLine,InfoLine,MsgIdLine,SubjectLine,DeliveryLine,RemovedFromQueueLine,PostfixLine

class LogFactory:
    @staticmethod
    def build_line(log_line: str) -> PostfixLine:
        result = None
        lst = [
            (ConnectLine.PATTERN, LogFactory._build_connect_line),
            (SmtpClientLine.PATTERN, LogFactory._build_client_line),
            (InfoLine.PATTERN, LogFactory._build_info_line),
            (MsgIdLine.PATTERN, LogFactory._build_msgid_line),
            (SubjectLine.PATTERN, LogFactory._build_subj_line),
            (DeliveryLine.PATTERN, LogFactory._build_delivery_line),
            (RemovedFromQueueLine.PATTERN, LogFactory._build_removed_line)
        ]
        for item in lst:
            match = re.match(item[0], log_line)
            if match:
                result = item[1](match)
        return result

    @staticmethod
    def _build_connect_line(matchRes: Match) -> ConnectLine:
        return ConnectLine(matchRes)

    @staticmethod
    def _build_client_line(matchRes: Match) -> SmtpClientLine:
        return SmtpClientLine(matchRes)

    @staticmethod
    def _build_info_line(matchRes: Match) -> InfoLine:
        return InfoLine(matchRes)

    @staticmethod
    def _build_msgid_line(matchRes: Match) -> MsgIdLine:
        return MsgIdLine(matchRes)

    @staticmethod
    def _build_subj_line(matchRes: Match) -> SubjectLine:
        return SubjectLine(matchRes)

    @staticmethod
    def _build_delivery_line(matchRes: Match) -> DeliveryLine:
        return DeliveryLine(matchRes)

    @staticmethod
    def _build_removed_line(matchRes: Match) -> RemovedFromQueueLine:
        return RemovedFromQueueLine(matchRes)
