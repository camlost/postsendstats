# mailrep #
## Description ##

The program is intended to process postfix log file(s).
It processes a file line by line to collect complete information about a message - when 
it was received, what was the sender, recipients, size and so on and when it was
sent with what result.

Based on command line arguments it should filter messages to process only the relevant ones.

## Available filters ##

Messages can be filtered by:
  - sender and / or recipient data (substring match, i.e. domain)
  - message ID
  - subject
  - DSN
  - and possibly other data such as target server IP address

## Working modes ##

The following modes are available:
- Per minute outgoing traffic statistics containing number of messages sent successfully,
  number of retries and number of fails together with total message size
- Per minute incoming traffic statistics with number of messages and size data
- Message tracking 

## Output ##

Output can be written into a specified CSV file or sent to stdout (default).

## Usage ##
```
mailrep.py [mode] <filters> [output]

  mode:
    -h    Display help
    -is   Incoming statistics
    -os   Outgoing statistics
    -mt   Message tracking 

  filters:
    -d    Date range start (included)
    -e    Date range end (excluded)
    -i    Message ID
    -f    From / sender
    -t    To / recipient
    -s    Subject

  output:
    -o          CSV path
    --append    Append output to the file specified by -o parameter
    stdout if nothing specified
```
Example:

mailrep.py -mt -d 2019-06-20 -e 2019-06-22 -f postmaster@domain.com -t @example.net -o tracking.csv --apend