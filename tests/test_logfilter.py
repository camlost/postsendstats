import pytest

from libpostlog.messagedata import MessageData
from libpostlog.logfilter import LogStringFilter

def test_stringfilter():
    o = MessageData()
    o.subject = "Test subject"
    f = LogStringFilter("subject", "eSt")
    assert f.match(o) == True
    