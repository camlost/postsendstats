from distutils.core import setup

setup(
    name='PostfixSendStats',
    author='Jiri Novak',
    author_email='jiri.novak@gmail.com',
    url='https://bitbucket.org/camlost/postsendstats',
    version='0.1dev',
    package_dir={
        'postfixsendstats': 'src',
        'libpostlog': 'src/libpostlog'
    },
    packages=['postfixsendstats', 'libpostlog'],
    license='Creative Commons Attribution-Noncommercial-Share Alike license',
    long_description=open('README.txt').read(),
)