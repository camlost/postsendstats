# mailrep Workflow #

- Read data from config file (i.e. postfix log path and file mask)
- Read command line arguments (precedence over config file)
- Create a file mask based on time range specified
- Process each file:
  - Collect complete information about a message handling
  - Filter messages based on filtering criteria
- Build output data
- Write the data to specified output - a file or stdout